"""
To be used with ipython --matplotlib

run job_sim_predaprey.py

"""
from fluidsim.solvers.models0d.predaprey.solver import Simul

import matplotlib.pyplot as plt

params = Simul.create_default_params()

params.time_stepping.deltat0 = 1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var('X', sim.Xs + 2.)
sim.state.state_phys.set_var('Y', sim.Ys + 1.)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
plt.savefig('XY_ts=1_tend=20.png')
sim.output.print_stdout.plot_XY_vs_time()
plt.savefig('XY_vs_time_ts=1_tend=20.png')



