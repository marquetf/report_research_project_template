"""
To be used with ipython --matplotlib

run job_sim_lorenz.py

"""

from fluidsim.solvers.models0d.lorenz.solver import Simul

import matplotlib.pyplot as plt

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.02
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var('X', sim.Xs0 + 2.)
sim.state.state_phys.set_var('Y', sim.Ys0 - 1.)
sim.state.state_phys.set_var('Z', sim.Zs0 - 10.)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
plt.savefig('XY.png')
sim.output.print_stdout.plot_XZ()
plt.savefig('XZ.png')
